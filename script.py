from luper import Luper
import json

with open('./filestorage/output.json', 'r') as file:
    ds = json.load(file)

out = [Luper.doit(e) for e in ds]

with open('./filestorage/final.json', 'w') as file:
    json.dump(out, file)
